// MontyHall.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <time.h>
#include <iostream>
#include <random>
using namespace std;

int roll(int min, int max)
{
	return rand() % (max - min + 1) + min;
}

int main()
{
	int rounds       = 100;
	int win          = 0;
	int lose         = 0;
	int winningDoor  = 0;
	int playerChoice = 1;
	int revealedDoor = 0;

	srand(time(NULL));

	for (int i = 1; i <= rounds; i++)
	{
		winningDoor  = roll(1, 3);
		playerChoice = roll(1, 3);

		#pragma region playerSwitches
		//if (playerChoice == winningDoor)
		//{
		//	//1
		//	if (playerChoice == 1)
		//	{
		//		revealedDoor = roll(2, 3);
		//		if (revealedDoor == 2)
		//			playerChoice = 3;
		//		else //revealedDoor == 3
		//			playerChoice = 2;
		//	}
		//	//2
		//	else if (playerChoice == 2)
		//	{
		//		revealedDoor = roll(1, 2);
		//		if (revealedDoor == 1)
		//			playerChoice = 3;
		//		else // revealedDoor == 2, which we treat as door number 3
		//			playerChoice = 1;
		//	}
		//	//3
		//	else
		//	{
		//		revealedDoor = roll(1, 2);
		//		if (revealedDoor == 1)
		//			playerChoice = 2;
		//		else //revealedDoor == 2
		//			playerChoice = 1;
		//	}
		//}
		//else //playerChoice != winningDoor
		//{
		//	playerChoice = winningDoor;
		//}
		#pragma endregion

		if (playerChoice == winningDoor)
			win++;
		else
			lose++;
	}

	cout << "Win         : " << win << endl;
	cout << "Lose        : " << lose << endl;
	cout << "Total       : " << win + lose << endl;
	cout << "Win %       : " << double(win) / double(rounds) * 100 << endl;
	//cout << "Winning Door: " << winningDoor << endl;
	//cout << "PlayerChoice: " << playerChoice << endl;

    return 0;
}

